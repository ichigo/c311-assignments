## c311-assignments

a | 
--- | ---
Assignment 0 |  Be familiar with racket 
Assignment 1 | Recursion and Higher-Order Functional Abstractions
Assignment 2 | Free, Bound variables
Assignment 3 | Lexical Addressing, Environments and Interpreters
Assignment 4 | Environments and Interpreters
Assignment 5 | Parameter-Passing Conventions
Assignment 6 | Continuation-Passing Style
Assignment 7 | Continuations and Representation Independence
Assignment 8 | Registerization and Trampolining
Assignment 9 | ParentheC Interpreter
Assignment 10| Introduction to Logic Programming
Assignment 11| Type Inference
Assignment 12| Introduction to Monads

課程老師云：「Please do not post solutions to the exercises in a public place.」 
但我覺得對於自学還是有必要的。之前知乎 someone 兄寫的的部分代码幫助我點通了一下 Representation Independence，否則一直卡在百思不得其解的地方。思而不學則怠了。
