#lang racket

;; an interpreter that argument-passing is call-by-value.
;; add cons^ that evaluate its arguments lazily.

;; author: YaoSen Wang

(define empty-env
  (lambda ()
    (lambda(y)
      (error 'value-of "unbound variable ~s" y))))


(define extend-env
  (lambda (y v old-env)
    (lambda (query)                   
      (if (eqv? query y)
          v                         
          (apply-env old-env query)))))


(define apply-env
  (lambda (env y)
    (env y)))


(define make-closure
  (lambda (x body env)
    (lambda (arg)
      (val-of-cbv body (extend-env x (box arg) env)))))


(define apply-closure
  (lambda (c arg)
    (c arg)))


(define val-of-cbv
  (lambda (exp env)
    (printf "~a~n" exp)
    (match exp
      [`,b #:when (boolean? b) b]
           
      [`,n #:when (number? n)  n]

      [`,l #:when (eqv? '() l) '()]   

      [`(null? ,l)
       (null? (val-of-cbv l env))]
      
      [`(zero? ,n)
       (zero? (val-of-cbv n env))]

      [`(cons^ ,a ,b)
       `(,(lambda () (val-of-cbv a env)) . ,(lambda () (val-of-cbv b env)))]

      [`(car^ ,c)
       ((car (val-of-cbv c env)))]

      [`(cdr^ ,c)
       ((cdr (val-of-cbv c env)))]
      
      [`(cons ,a ,b)
       `(,(val-of-cbv a env) . ,(val-of-cbv b env))]

      [`(car ,c)
       (car (val-of-cbv c env))]

      [`(cdr ,c)
       (cdr (val-of-cbv c env))]

      [`(add1 ,n)
       (add1 (val-of-cbv n env))]
      
      [`(sub1 ,n)
       (sub1 (val-of-cbv n env))]
           
      [`(* ,n1 ,n2)
       (* (val-of-cbv n1 env) (val-of-cbv n2 env))]
           
      [`(if ,test ,conseq ,alt) (if (val-of-cbv test env)
                                    (val-of-cbv conseq env)
                                    (val-of-cbv alt env))]

      [`(let ([,x ,e]) ,body)
       (let ([new-env (extend-env x (box (val-of-cbv e env)) env)])
         (val-of-cbv body new-env))]
           
      [`(begin2 ,e1 ,e2)
       (begin (val-of-cbv e1 env) (val-of-cbv e2 env))]

      [`(set! ,y ,e)
       (let ([b (apply-env env y)]
             [v (val-of-cbv e env)])
         (set-box! b v))]
           
      [`(random ,n) (random (val-of-cbv n env))]        
           
      [`,y #:when (symbol? y)
           (unbox (apply-env env y))]
      
      [`(lambda (,x) ,body)
       (make-closure x body env)]
      
      [`(,rator ,rand)
       (apply-closure (val-of-cbv rator env)
                      (val-of-cbv rand env))])))   


;; tests

(val-of-cbv '(cons (* 2 3) (cons 2 ()))
            (empty-env))
; (6 2)

(val-of-cbv '(car (cons (* 3 2) 1))
            (empty-env))
; 6

(val-of-cbv '(null? ())
            (empty-env))
; t

(val-of-cbv '(car^ (cons^ 1 2))
            (empty-env))
; 1

(define cons-test
  '(let ((fix (lambda (f)
                ((lambda (x) (f (lambda (v) ((x x) v))))
                 (lambda (x) (f (lambda (v) ((x x) v))))))))
     (let ((map (fix (lambda (map)
                       (lambda (f)
                         (lambda (l)
                           (if (null? l)
                               ()                   ; quote不太会处理,表达式中的空链表一律写成()
                               (cons^ (f (car^ l))
                                      ((map f) (cdr^ l))))))))))
       (let ((take (fix (lambda (take)
                          (lambda (l)
                            (lambda (n)
                              (if (zero? n)
                                  ()
                                  (cons (car^ l) 
                                        ((take (cdr^ l)) (sub1 n))))))))))
         ((take ((fix (lambda (m)
                        (lambda (i)
                          (cons^ 1 ((map (lambda (x) (add1 x))) (m i)))))) ; 惰性链表, 0可以替換成任何對象
                 0)) 6)))))

(val-of-cbv cons-test (empty-env))
; (1 2 3 4 5 6)
