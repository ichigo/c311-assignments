void *r__t__cr__t__, *r__t__vr__t__, *r__t__kr__t__, *r__t__envr__t__, *r__t__yr__t__, *r__t__expr__t__;

void (*pc)();

struct expr;
typedef struct expr expr;
struct expr {
  enum {
    _const_expr,
    _var_expr,
    _if_expr,
    _mult_expr,
    _subr1_expr,
    _zero_expr,
    _catch_expr,
    _throw_expr,
    _let_expr,
    _lambda_expr,
    _app_expr
  } tag;
  union {
    struct { void *_cexp; } _const;
    struct { void *_n; } _var;
    struct { void *_test; void *_conseq; void *_alt; } _if;
    struct { void *_nexpr1; void *_nexpr2; } _mult;
    struct { void *_nexp; } _subr1;
    struct { void *_nexp; } _zero;
    struct { void *_body; } _catch;
    struct { void *_kexp; void *_vexp; } _throw;
    struct { void *_exp; void *_body; } _let;
    struct { void *_body; } _lambda;
    struct { void *_rator; void *_rand; } _app;
  } u;
};

void *exprr_const(void *cexp);
void *exprr_var(void *n);
void *exprr_if(void *test, void *conseq, void *alt);
void *exprr_mult(void *nexpr1, void *nexpr2);
void *exprr_subr1(void *nexp);
void *exprr_zero(void *nexp);
void *exprr_catch(void *body);
void *exprr_throw(void *kexp, void *vexp);
void *exprr_let(void *exp, void *body);
void *exprr_lambda(void *body);
void *exprr_app(void *rator, void *rand);

struct clos;
typedef struct clos clos;
struct clos {
  enum {
    _extend_clos
  } tag;
  union {
    struct { void *_func; void *_env; } _extend;
  } u;
};

void *closr_extend(void *func, void *env);

void applyr__m__closure();
struct envr;
typedef struct envr envr;
struct envr {
  enum {
    _empty_envr,
    _extend_envr
  } tag;
  union {
    struct { char dummy; } _empty;
    struct { void *_v; void *_env; } _extend;
  } u;
};

void *envrr_empty();
void *envrr_extend(void *v, void *env);

void applyr__m__env();
struct kt;
typedef struct kt kt;
struct kt {
  enum {
    _empty_kt,
    _r__t__r__m__inner_kt,
    _r__t__r__m__outer_kt,
    _subr1_kt,
    _zero_kt,
    _if_kt,
    _throwr__m__inner_kt,
    _throwr__m__outer_kt,
    _let_kt,
    _appr__m__inner_kt,
    _appr__m__outer_kt
  } tag;
  union {
    struct { void *_var; } _empty;
    struct { void *_vr__ex__; void *_k; } _r__t__r__m__inner;
    struct { void *_x; void *_env; void *_k; } _r__t__r__m__outer;
    struct { void *_k; } _subr1;
    struct { void *_k; } _zero;
    struct { void *_conseq; void *_alt; void *_env; void *_k; } _if;
    struct { void *_vr__ex__; } _throwr__m__inner;
    struct { void *_vr__ex__; void *_env; } _throwr__m__outer;
    struct { void *_vr__ex__; void *_env; void *_k; } _let;
    struct { void *_vr__ex__; void *_k; } _appr__m__inner;
    struct { void *_vr__ex__; void *_env; void *_k; } _appr__m__outer;
  } u;
};

void *ktr_empty(void *var);
void *ktr_r__t__r__m__inner(void *vr__ex__, void *k);
void *ktr_r__t__r__m__outer(void *x, void *env, void *k);
void *ktr_subr1(void *k);
void *ktr_zero(void *k);
void *ktr_if(void *conseq, void *alt, void *env, void *k);
void *ktr_throwr__m__inner(void *vr__ex__);
void *ktr_throwr__m__outer(void *vr__ex__, void *env);
void *ktr_let(void *vr__ex__, void *env, void *k);
void *ktr_appr__m__inner(void *vr__ex__, void *k);
void *ktr_appr__m__outer(void *vr__ex__, void *env, void *k);

void applyr__m__k();
void valuer__m__ofr__m__cps();
int main();
int mount_tram();

struct _trstr;
typedef struct _trstr _trstr;
struct _trstr {
  jmp_buf *jmpbuf;
  int value;
};

