#include <time.h>
#include <setjmp.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "a9.h"

void *ktr_empty(void *var)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _empty_kt;
  _data->u._empty._var = var;
  return (void *)_data;
}

void *ktr_r__t__r__m__inner(void *vr__ex__, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _r__t__r__m__inner_kt;
  _data->u._r__t__r__m__inner._vr__ex__ = vr__ex__;
  _data->u._r__t__r__m__inner._k = k;
  return (void *)_data;
}

void *ktr_r__t__r__m__outer(void *x, void *env, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _r__t__r__m__outer_kt;
  _data->u._r__t__r__m__outer._x = x;
  _data->u._r__t__r__m__outer._env = env;
  _data->u._r__t__r__m__outer._k = k;
  return (void *)_data;
}

void *ktr_subr1(void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _subr1_kt;
  _data->u._subr1._k = k;
  return (void *)_data;
}

void *ktr_zero(void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _zero_kt;
  _data->u._zero._k = k;
  return (void *)_data;
}

void *ktr_if(void *conseq, void *alt, void *env, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _if_kt;
  _data->u._if._conseq = conseq;
  _data->u._if._alt = alt;
  _data->u._if._env = env;
  _data->u._if._k = k;
  return (void *)_data;
}

void *ktr_throwr__m__inner(void *vr__ex__)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _throwr__m__inner_kt;
  _data->u._throwr__m__inner._vr__ex__ = vr__ex__;
  return (void *)_data;
}

void *ktr_throwr__m__outer(void *vr__ex__, void *env)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _throwr__m__outer_kt;
  _data->u._throwr__m__outer._vr__ex__ = vr__ex__;
  _data->u._throwr__m__outer._env = env;
  return (void *)_data;
}

void *ktr_let(void *vr__ex__, void *env, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _let_kt;
  _data->u._let._vr__ex__ = vr__ex__;
  _data->u._let._env = env;
  _data->u._let._k = k;
  return (void *)_data;
}

void *ktr_appr__m__inner(void *vr__ex__, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _appr__m__inner_kt;
  _data->u._appr__m__inner._vr__ex__ = vr__ex__;
  _data->u._appr__m__inner._k = k;
  return (void *)_data;
}

void *ktr_appr__m__outer(void *vr__ex__, void *env, void *k)
{
  kt *_data = (kt *)malloc(sizeof(kt));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _appr__m__outer_kt;
  _data->u._appr__m__outer._vr__ex__ = vr__ex__;
  _data->u._appr__m__outer._env = env;
  _data->u._appr__m__outer._k = k;
  return (void *)_data;
}

void *envrr_empty()
{
  envr *_data = (envr *)malloc(sizeof(envr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _empty_envr;
  return (void *)_data;
}

void *envrr_extend(void *v, void *env)
{
  envr *_data = (envr *)malloc(sizeof(envr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _extend_envr;
  _data->u._extend._v = v;
  _data->u._extend._env = env;
  return (void *)_data;
}

void *closr_extend(void *func, void *env)
{
  clos *_data = (clos *)malloc(sizeof(clos));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _extend_clos;
  _data->u._extend._func = func;
  _data->u._extend._env = env;
  return (void *)_data;
}

void *exprr_const(void *cexp)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _const_expr;
  _data->u._const._cexp = cexp;
  return (void *)_data;
}

void *exprr_var(void *n)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _var_expr;
  _data->u._var._n = n;
  return (void *)_data;
}

void *exprr_if(void *test, void *conseq, void *alt)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _if_expr;
  _data->u._if._test = test;
  _data->u._if._conseq = conseq;
  _data->u._if._alt = alt;
  return (void *)_data;
}

void *exprr_mult(void *nexpr1, void *nexpr2)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _mult_expr;
  _data->u._mult._nexpr1 = nexpr1;
  _data->u._mult._nexpr2 = nexpr2;
  return (void *)_data;
}

void *exprr_subr1(void *nexp)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _subr1_expr;
  _data->u._subr1._nexp = nexp;
  return (void *)_data;
}

void *exprr_zero(void *nexp)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _zero_expr;
  _data->u._zero._nexp = nexp;
  return (void *)_data;
}

void *exprr_catch(void *body)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _catch_expr;
  _data->u._catch._body = body;
  return (void *)_data;
}

void *exprr_throw(void *kexp, void *vexp)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _throw_expr;
  _data->u._throw._kexp = kexp;
  _data->u._throw._vexp = vexp;
  return (void *)_data;
}

void *exprr_let(void *exp, void *body)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _let_expr;
  _data->u._let._exp = exp;
  _data->u._let._body = body;
  return (void *)_data;
}

void *exprr_lambda(void *body)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _lambda_expr;
  _data->u._lambda._body = body;
  return (void *)_data;
}

void *exprr_app(void *rator, void *rand)
{
  expr *_data = (expr *)malloc(sizeof(expr));
  if (!_data)
  {
    fprintf(stderr, "Out of memory\n");
    exit(1);
  }
  _data->tag = _app_expr;
  _data->u._app._rator = rator;
  _data->u._app._rand = rand;
  return (void *)_data;
}

int main()
{
  r__t__expr__t__ = (void *)exprr_let(exprr_lambda(exprr_lambda(exprr_if(exprr_zero(exprr_var((void *)0)), exprr_const((void *)1), exprr_mult(exprr_var((void *)0), exprr_app(exprr_app(exprr_var((void *)1), exprr_var((void *)1)), exprr_subr1(exprr_var((void *)0))))))), exprr_mult(exprr_catch(exprr_app(exprr_app(exprr_var((void *)1), exprr_var((void *)1)), exprr_throw(exprr_var((void *)0), exprr_app(exprr_app(exprr_var((void *)1), exprr_var((void *)1)), exprr_const((void *)4))))), exprr_const((void *)5)));
  r__t__envr__t__ = (void *)envrr_empty();
  pc = &valuer__m__ofr__m__cps;
  mount_tram();
  printf("Fact 5: %d\n", (int)r__t__vr__t__);
}

void valuer__m__ofr__m__cps()
{
  expr *_c = (expr *)r__t__expr__t__;
  switch (_c->tag)
  {
  case _const_expr:
  {
    void *cexp = _c->u._const._cexp;
    r__t__vr__t__ = (void *)cexp;
    pc = &applyr__m__k;
    break;
  }
  case _mult_expr:
  {
    void *nexpr1 = _c->u._mult._nexpr1;
    void *nexpr2 = _c->u._mult._nexpr2;
    r__t__expr__t__ = (void *)nexpr1;
    r__t__kr__t__ = (void *)ktr_r__t__r__m__outer(nexpr2, r__t__envr__t__, r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _subr1_expr:
  {
    void *nexp = _c->u._subr1._nexp;
    r__t__expr__t__ = (void *)nexp;
    r__t__kr__t__ = (void *)ktr_subr1(r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _zero_expr:
  {
    void *nexp = _c->u._zero._nexp;
    r__t__expr__t__ = (void *)nexp;
    r__t__kr__t__ = (void *)ktr_zero(r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _if_expr:
  {
    void *test = _c->u._if._test;
    void *conseq = _c->u._if._conseq;
    void *alt = _c->u._if._alt;
    r__t__expr__t__ = (void *)test;
    r__t__kr__t__ = (void *)ktr_if(conseq, alt, r__t__envr__t__, r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _catch_expr:
  {
    void *body = _c->u._catch._body;
    r__t__expr__t__ = (void *)body;
    r__t__envr__t__ = (void *)envrr_extend(r__t__kr__t__, r__t__envr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _throw_expr:
  {
    void *kexp = _c->u._throw._kexp;
    void *vexp = _c->u._throw._vexp;
    r__t__expr__t__ = (void *)kexp;
    r__t__kr__t__ = (void *)ktr_throwr__m__outer(vexp, r__t__envr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _let_expr:
  {
    void *exp = _c->u._let._exp;
    void *body = _c->u._let._body;
    r__t__expr__t__ = (void *)exp;
    r__t__kr__t__ = (void *)ktr_let(body, r__t__envr__t__, r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _var_expr:
  {
    void *n = _c->u._var._n;
    r__t__yr__t__ = (void *)n;
    pc = &applyr__m__env;
    break;
  }
  case _lambda_expr:
  {
    void *body = _c->u._lambda._body;
    r__t__vr__t__ = (void *)closr_extend(body, r__t__envr__t__);
    pc = &applyr__m__k;
    break;
  }
  case _app_expr:
  {
    void *rator = _c->u._app._rator;
    void *rand = _c->u._app._rand;
    r__t__expr__t__ = (void *)rator;
    r__t__kr__t__ = (void *)ktr_appr__m__outer(rand, r__t__envr__t__, r__t__kr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  }
}

void applyr__m__k()
{
  kt *_c = (kt *)r__t__kr__t__;
  switch (_c->tag)
  {
  case _empty_kt:
  {
    void *var = _c->u._empty._var;
    _trstr *trstr = (_trstr *)var;
    longjmp(*trstr->jmpbuf, 1);
    break;
  }
  case _r__t__r__m__inner_kt:
  {
    void *vr__ex__ = _c->u._r__t__r__m__inner._vr__ex__;
    void *k = _c->u._r__t__r__m__inner._k;
    r__t__kr__t__ = (void *)k;
    r__t__vr__t__ = (void *)(void *)((int)vr__ex__ * (int)r__t__vr__t__);
    pc = &applyr__m__k;
    break;
  }
  case _r__t__r__m__outer_kt:
  {
    void *x = _c->u._r__t__r__m__outer._x;
    void *env = _c->u._r__t__r__m__outer._env;
    void *k = _c->u._r__t__r__m__outer._k;
    r__t__expr__t__ = (void *)x;
    r__t__envr__t__ = (void *)env;
    r__t__kr__t__ = (void *)ktr_r__t__r__m__inner(r__t__vr__t__, k);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _subr1_kt:
  {
    void *k = _c->u._subr1._k;
    r__t__kr__t__ = (void *)k;
    r__t__vr__t__ = (void *)(void *)((int)r__t__vr__t__ - 1);
    pc = &applyr__m__k;
    break;
  }
  case _zero_kt:
  {
    void *k = _c->u._zero._k;
    r__t__kr__t__ = (void *)k;
    r__t__vr__t__ = (void *)(r__t__vr__t__ == 0);
    pc = &applyr__m__k;
    break;
  }
  case _if_kt:
  {
    void *conseq = _c->u._if._conseq;
    void *alt = _c->u._if._alt;
    void *env = _c->u._if._env;
    void *k = _c->u._if._k;
    if (r__t__vr__t__)
    {
      r__t__expr__t__ = (void *)conseq;
      r__t__envr__t__ = (void *)env;
      r__t__kr__t__ = (void *)k;
      pc = &valuer__m__ofr__m__cps;
    }
    else
    {
      r__t__expr__t__ = (void *)alt;
      r__t__envr__t__ = (void *)env;
      r__t__kr__t__ = (void *)k;
      pc = &valuer__m__ofr__m__cps;
    }
    break;
  }
  case _throwr__m__inner_kt:
  {
    void *vr__ex__ = _c->u._throwr__m__inner._vr__ex__;
    r__t__kr__t__ = (void *)vr__ex__;
    pc = &applyr__m__k;
    break;
  }
  case _throwr__m__outer_kt:
  {
    void *vr__ex__ = _c->u._throwr__m__outer._vr__ex__;
    void *env = _c->u._throwr__m__outer._env;
    r__t__expr__t__ = (void *)vr__ex__;
    r__t__envr__t__ = (void *)env;
    r__t__kr__t__ = (void *)ktr_throwr__m__inner(r__t__vr__t__);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _let_kt:
  {
    void *vr__ex__ = _c->u._let._vr__ex__;
    void *env = _c->u._let._env;
    void *k = _c->u._let._k;
    r__t__expr__t__ = (void *)vr__ex__;
    r__t__envr__t__ = (void *)envrr_extend(r__t__vr__t__, env);
    r__t__kr__t__ = (void *)k;
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  case _appr__m__inner_kt:
  {
    void *vr__ex__ = _c->u._appr__m__inner._vr__ex__;
    void *k = _c->u._appr__m__inner._k;
    r__t__cr__t__ = (void *)vr__ex__;
    r__t__kr__t__ = (void *)k;
    pc = &applyr__m__closure;
    break;
  }
  case _appr__m__outer_kt:
  {
    void *vr__ex__ = _c->u._appr__m__outer._vr__ex__;
    void *env = _c->u._appr__m__outer._env;
    void *k = _c->u._appr__m__outer._k;
    r__t__expr__t__ = (void *)vr__ex__;
    r__t__envr__t__ = (void *)env;
    r__t__kr__t__ = (void *)ktr_appr__m__inner(r__t__vr__t__, k);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  }
}

void applyr__m__env()
{
  envr *_c = (envr *)r__t__envr__t__;
  switch (_c->tag)
  {
  case _empty_envr:
  {
    fprintf(stderr, "unbound identifier");
    exit(1);
    break;
  }
  case _extend_envr:
  {
    void *vr__ex__ = _c->u._extend._v;
    void *envr__ex__ = _c->u._extend._env;
    if ((r__t__yr__t__ == 0))
    {
      r__t__vr__t__ = (void *)vr__ex__;
      pc = &applyr__m__k;
    }
    else
    {
      r__t__envr__t__ = (void *)envr__ex__;
      r__t__yr__t__ = (void *)(void *)((int)r__t__yr__t__ - 1);
      pc = &applyr__m__env;
    }
    break;
  }
  }
}

void applyr__m__closure()
{
  clos *_c = (clos *)r__t__cr__t__;
  switch (_c->tag)
  {
  case _extend_clos:
  {
    void *func = _c->u._extend._func;
    void *env = _c->u._extend._env;
    r__t__expr__t__ = (void *)func;
    r__t__envr__t__ = (void *)envrr_extend(r__t__vr__t__, env);
    pc = &valuer__m__ofr__m__cps;
    break;
  }
  }
}

int mount_tram()
{
  srand(time(NULL));
  jmp_buf jb;
  _trstr trstr;
  void *dismount;
  int _status = setjmp(jb);
  trstr.jmpbuf = &jb;
  dismount = &trstr;
  if (!_status)
  {
    r__t__kr__t__ = (void *)ktr_empty(dismount);
    for (;;)
    {
      pc();
    }
  }
  return 0;
}
